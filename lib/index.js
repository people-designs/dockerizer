const chalk = require('chalk');
const commonTags = require('common-tags');
const { validate: isValidEmail } = require('email-validator');
const fs = require('fs');
const Gitlab = require('gitlab/dist/es5').default;
const handlebars = require('handlebars');
const inquirer = require('inquirer');
const isValidDomain = require('is-valid-domain');
const path = require('path');
const shell = require('shelljs');

const templates = require('../templates');

const run = async ({
  dryRun = false,
  gitlab = false,
  region = 'us-central1',
  sql = false,
  sqlConnection = 'pd-secure:us-central1:sql-server-1',
  verbose = false,
  zone = 'us-central1-a',
}) => {
  const exitsSuccessfully = command => shell.exec(command, { silent: !verbose }).code === 0;

  const error = message => console.log(chalk.red(commonTags.stripIndent(message)));
  const info = message => console.log(chalk.cyan(commonTags.stripIndent(message)));
  const success = message => console.log(chalk.green(commonTags.stripIndent(message)));
  const warning = message => console.log(chalk.yellow(commonTags.stripIndent(message)));

  const projectIsAvailable = ({ shouldCreateProject }) => shouldCreateProject !== false;

  if (!shell.which('gcloud')) {
    return error('gcloud not found.');
  }

  if (!shell.which('docker-machine')) {
    return error('docker-machine not found.');
  }

  if (!exitsSuccessfully('gcloud config list account --format "value(core.account)"')) {
    return error(`
      You do not currently have an active Google account selected. Please run:

      $ gcloud auth login
    `);
  }

  const answers = await inquirer.prompt([
    {
      name: 'projectName',
      type: 'input',
      message: 'Google project name? If using ID instead, put in [brackets].',
      default: path.basename(process.cwd()),
    },
    {
      name: 'shouldCreateProject',
      type: 'confirm',
      message: 'Google project does not exist. Create it?',
      when: dryRun ? false : ({ projectName }) => (
        /\[.*\]/.test(projectName)
          ? !exitsSuccessfully(`gcloud projects describe ${projectName.slice(1, -1)}`)
          : shell.exec(`gcloud projects list --filter="NAME=${projectName}" --format="value(NAME)"`, { silent: true }).stdout.trim().length === 0
      ),
    },
    {
      name: 'subdirectory',
      type: 'input',
      message: 'Subdirectory?',
      when: projectIsAvailable,
      validate: input => (input.length && fs.existsSync(path.join(process.cwd(), input)))
        || 'Subdirectory not found. Try again.',
    },
    {
      name: 'instanceName',
      type: 'input',
      message: 'Instance name?',
      when: projectIsAvailable,
      default: ({ projectName, subdirectory }) => `${projectName}-${subdirectory}`,
      validate: input => /(?:[a-z](?:[-a-z0-9]{0,61}[a-z0-9])?)/.test(input)
        || 'Invalid instance name. Try again.',
    },
    {
      name: 'machineType',
      type: 'list',
      message: 'Machine type?',
      choices: JSON.parse(shell.exec(`gcloud compute machine-types list --filter="zone:(${zone})" --format="json"`, { silent: true }).stdout)
        .sort((a, b) => parseInt(a.id, 10) - parseInt(b.id, 10))
        .map(machineType => machineType.name),
      default: 'f1-micro',
      when: projectIsAvailable,
    },
    {
      name: 'host',
      type: 'input',
      message: 'Host name?',
      when: projectIsAvailable,
      validate: input => isValidDomain(input)
        || 'Invalid host name. Try again.',
    },
    {
      name: 'letsencryptEmail',
      type: 'input',
      message: 'Let\'s Encrypt email address?',
      when: projectIsAvailable,
      default: 'jwall@peopledesigns.com',
      validate: input => isValidEmail(input)
        || 'Invalid email address. Try again.',
    },
    {
      name: 'buildComponents',
      type: 'input',
      message: 'Build components? [separate by spaces]',
      when: projectIsAvailable,
      default: 'laravel-php laravel-frontend laravel-app',
      filter: input => input.match(/\S+/g) || [],
      validate: input => input.every(c => Object.keys(templates.Dockerfile).includes(c))
        || `Unsupported build component. Options are ${Object.keys(templates.Dockerfile).join(',')}.`,
    },
    {
      name: 'secrets',
      type: 'input',
      message: 'Secrets? [separate by spaces]',
      when: projectIsAvailable,
      default: sql ? 'db_database db_username app_key' : 'db_host db_database db_username db_password app_key',
      filter: input => input.match(/\S+/g) || [],
      validate: sql ? input => (input.includes('db_database') && input.includes('db_username'))
        || 'db_database and db_username required when using --sql.' : undefined,
    },
  ]);

  if (!projectIsAvailable(answers)) {
    return error('Google project not available. Exiting now.');
  }

  answers.buildStages = answers.buildComponents.filter(c => !c.includes('-app'));

  const secretAnswers = await inquirer.prompt(
    answers.secrets.filter(
      secret => secret !== 'app_key' && (!sql || secret !== 'db_password'),
    ).map(secret => ({
      name: secret,
      type: (secret.endsWith('secret') || secret.endsWith('password')) ? 'password' : 'input',
      message: secret,
      validate: input => input.length > 0,
    })),
  );

  if (answers.secrets.includes('app_key')) {
    secretAnswers.app_key = shell.exec('printf %s base64:`openssl rand -base64 32`', { silent: true }).stdout;
  }

  if (sql) {
    secretAnswers.db_password = shell.exec('printf %s `openssl rand -base64 32`', { silent: true }).stdout;
    answers.secrets = [...answers.secrets, 'db_password'];
  }

  if (/\[.*\]/.test(answers.projectName)) {
    answers.projectName = answers.projectName.slice(1, -1);
    answers.projectId = answers.projectName;
  } else {
    answers.projectId = shell.exec(`gcloud projects list --filter="NAME=${answers.projectName}" --format="value(PROJECT_ID)"`, { silent: true }).stdout.trim()
      || `pd-${answers.projectName}-${Date.now().toString().substr(-6)}`;
  }

  const {
    buildComponents,
    buildStages,
    instanceName,
    machineType,
    projectId,
    projectName,
    secrets,
    shouldCreateProject,
    subdirectory,
  } = answers;

  if (shouldCreateProject) {
    const createProjectCommand = `gcloud projects create ${answers.projectId} --name ${answers.projectName}
      && gcloud beta billing projects link ${answers.projectId} --billing-account=\`gcloud beta billing accounts list --filter="NAME='PD Billing Account'" --format="value(ACCOUNT_ID)"\`
      && gcloud services enable compute.googleapis.com --project ${answers.projectId}
      && gcloud services enable sqladmin.googleapis.com --project ${answers.projectId}
    `;
    if (dryRun) {
      info(`Dry run. Skipping command: \n${createProjectCommand}\n\n`);
    } else if (shell.exec(commonTags.oneLine(createProjectCommand)).code === 0) {
      success(`Successfully created Google project with name ${answers.projectName} and ID ${answers.projectId}.`);
    } else {
      return error('Unable to create project. Exiting now.');
    }
  }

  const createAddressCommand = `gcloud compute addresses create ${instanceName} --region ${region} --project ${projectId}`;
  if (dryRun) {
    info(`Dry run. Skipping command: \n${createAddressCommand}\n\n`);
  } else if (exitsSuccessfully(createAddressCommand)) {
    success(`Successfully created Google address ${instanceName}.`);
  } else {
    warning('Unable to create Google address. It may already exist.');
  }

  const createInstanceCommand = `docker-machine create ${instanceName} --driver google
    --google-project ${projectId}
    --google-zone ${zone}
    --google-machine-type ${machineType}
    --google-tags http-server,https-server
    --google-address ${instanceName}
    --google-scopes https://www.googleapis.com/auth/sqlservice.admin,https://www.googleapis.com/auth/devstorage.read_write
  `;
  if (dryRun) {
    info(`Dry run. Skipping command: \n${createInstanceCommand}\n\n`);
  } else if (shell.exec(commonTags.oneLine(createInstanceCommand)).code === 0) {
    success(`Successfully created docker-machine instance ${instanceName}.`);
  } else {
    return error('Unable to create docker-machine instance. It may already exist.');
  }

  const instanceServiceAccount = shell.exec(`printf %s $(gcloud compute instances describe ${instanceName} --project ${projectId} --format="value(serviceAccounts.email)" --zone=${zone})`, { silent: true }).stdout;

  const instanceIp = shell.exec(`printf %s $(docker-machine ip ${instanceName})`, { silent: true }).stdout;

  info(`Instance IP address is ${instanceIp}.`);

  if (sql) {
    if (dryRun) {
      info('Dry run. Skipping SQL commands.');
    } else {
      const [sqlProject, , sqlInstance] = sqlConnection.split(':');
      const sqlHost = JSON.parse(shell.exec(`gcloud sql instances describe ${sqlInstance} --project ${sqlProject} --format=json`, { silent: true }).stdout).ipAddresses.find(a => a.type === 'PRIMARY').ipAddress.trim();
      const { sqlUser } = await inquirer.prompt([
        {
          name: 'sqlUser',
          type: 'input',
          message: 'Google SQL user for creating account?',
        },
      ]);
      if (exitsSuccessfully(`mysql --host=${sqlHost} --user=${sqlUser} -p --execute "
        CREATE USER '${secretAnswers.db_username}'@'cloudsqlproxy~${instanceIp}'; \
        CREATE DATABASE IF NOT EXISTS \\\`${secretAnswers.db_database}\\\`; \
        GRANT ALL PRIVILEGES ON \\\`${secretAnswers.db_database}\\\`.* to '${secretAnswers.db_username}'@'cloudsqlproxy~${instanceIp}' IDENTIFIED BY '${secretAnswers.db_password}'; \
      "`)) {
        success(`Successfully created SQL database ${secretAnswers.db_database} and SQL user ${secretAnswers.db_username}.`);
      } else {
        return error(`Unable to create SQL database ${secretAnswers.db_database} and SQL user ${secretAnswers.db_username}.`);
      }
      if (exitsSuccessfully(`gcloud projects add-iam-policy-binding ${sqlProject} --member serviceAccount:${instanceServiceAccount} --role roles/cloudsql.editor`)) {
        success(`Successfully added IAM policy to ${sqlConnection}.`);
      } else {
        return error(`Unable to add IAM policy to ${sqlConnection}.`);
      }
    }
  }

  if (dryRun) {
    info('Dry run. Skipping commands on docker-machine instance.');
  } else {
    if (shell.exec(`docker-machine ssh ${instanceName} '
      set +o history
      sudo mkdir -p /opt/traefik
      sudo touch /opt/traefik/acme.json
      sudo chmod 600 /opt/traefik/acme.json
      sudo apt-get -y update
      sudo apt-get -y install mysql-client
    '`, { silent: !verbose }).code === 0) {
      success(`Successfully prepared docker-machine instance ${instanceName}.`);
    } else {
      return error('Unable to prepare docker-machine instance.');
    }

    const env = shell.exec(`docker-machine env ${instanceName}`, { silent: true }).stdout
      .split('\n')
      .map(line => /export (.*)/.exec(line))
      .reduce(
        (string, matches) => (matches ? `${matches[1]} ${string}` : string),
        '',
      );

    if (exitsSuccessfully(`${env} docker swarm init`)
      && exitsSuccessfully(`${env} docker network create -d overlay docker-socket-proxy-network`)
      && exitsSuccessfully(`${env} docker network create -d overlay app-network`)
      && Object.entries(secretAnswers).every(([k, v]) => exitsSuccessfully(`echo ${v} | ${env} docker secret create ${k} -`))
    ) {
      success(`Successfully configured docker on ${instanceName}.`);
    } else {
      return error('Unable to configure docker.');
    }
  }

  if (dryRun) {
    info('Dry run. Skipping file creation.');
  } else {
    if (!fs.existsSync('.gitlab-ci.yml')) {
      fs.writeFileSync(
        '.gitlab-ci.yml',
        handlebars.compile(templates['.gitlab-ci.yml'].project)(answers),
      );
    }

    if (!fs.readFileSync('.gitlab-ci.yml').includes(`- local: ${subdirectory}/.gitlab-ci.yml`)) {
      fs.appendFileSync(
        '.gitlab-ci.yml',
        `\n  - local: ${subdirectory}/.gitlab-ci.yml\n`,
      );
    }

    fs.writeFileSync(
      `${subdirectory}/.gitlab-ci.yml`,
      handlebars.compile(templates['.gitlab-ci.yml'].instance)({ ...answers, ...{ instanceIp, sqlConnection } }),
    );

    fs.writeFileSync(
      `${subdirectory}/Dockerfile`,
      buildComponents.map(c => handlebars.compile(templates.Dockerfile[c])(answers)).join('\n\n'),
    );

    fs.writeFileSync(
      `${subdirectory}/.dockerignore`,
      ['common', ...buildStages].map(c => handlebars.compile(templates['.dockerignore'][c])(answers)).join('\n\n'),
    );

    if (buildComponents.includes('laravel-app') || buildComponents.includes('laravel-app-elixir')) {
      fs.writeFileSync(
        `${subdirectory}/laravel/app/helpers.php`,
        templates['helpers.php'],
      );
      shell.exec(`perl -i -pe 's/env\\((.(?:${secrets.join('|').toUpperCase()}).)/env_or_docker_secret($1/g' ${subdirectory}/laravel/config/*`);
      const composer = JSON.parse(fs.readFileSync(`${subdirectory}/laravel/composer.json`));
      composer.autoload.files = ['app/helpers.php'];
      fs.writeFileSync(
        `${subdirectory}/laravel/composer.json`,
        JSON.stringify(composer, null, 2),
      );
    }
  }

  if (gitlab) {
    if (dryRun) {
      info('Dry run. Skipping commands.');
    } else {
      const { HostOptions: { AuthOptions: { CaCertPath: caCertPath, CaPrivateKeyPath: caPrivateKeyPath } } } = JSON.parse(shell.exec(`docker-machine inspect ${instanceName}`, { silent: true }).stdout);
      const ca = {
        key: `CA_${instanceName.toUpperCase().replace(/\W/g, '_')}`,
        value: shell.exec(`cat ${caCertPath}`, { silent: true }).stdout,
      };
      const clientKey = {
        key: `CLIENT_KEY_${instanceName.toUpperCase().replace(/\W/g, '_')}`,
        value: shell.exec('openssl genrsa', { silent: !verbose }).stdout,
      };
      const clientCert = {
        key: `CLIENT_CERT_${instanceName.toUpperCase().replace(/\W/g, '_')}`,
        value: shell.exec(`echo "${clientKey.value}" | ${__dirname}/../scripts/generate-client-certs.sh ${caCertPath} ${caPrivateKeyPath}`, { silent: !verbose }).stdout,
      };

      const { gitlabRepository, token } = await inquirer.prompt([
        {
          name: 'gitlabRepository',
          type: 'input',
          message: 'Gitlab repository?',
          default: `people-designs/${projectName}`,
        },
        {
          name: 'token',
          type: 'password',
          message: 'Gitlab private token for API usage?',
        },
      ]);

      const { ProjectVariables } = new Gitlab({ token });

      await Promise.all([ca, clientKey, clientCert].map(
        v => ProjectVariables.create(gitlabRepository, { ...v, ...{ protected: true } }),
      )).then(() => success('Successfully set Gitlab variables.')).catch(({ error: apiError, statusCode }) => {
        switch (statusCode) {
          case 400:
            return warning(`Gitlab error: ${apiError.message.key.join(' ')}`);
          case 401:
            return error(`Gitlab error: ${apiError.message}`);
          default:
            return error('Error setting Gitlab variables');
        }
      });
    }
  }

  return null;
};


exports.run = run;
