module.exports = {
  extends: 'airbnb-base',
  root: true,
  rules: {
    'global-require': 'off',
    'import/no-unresolved': [2, { caseSensitive: false }],
    'import/prefer-default-export': [0],
    'arrow-parens': ['error', 'as-needed'],
    'no-console': 'off',
    'no-restricted-syntax': [
      'error',
      {
        selector: "CallExpression[callee.object.name='console'][callee.property.name!=/^(log|warn|error|info|trace)$/]",
        message: "Unexpected property on console object was called",
      },
    ],
  },
  env: {
    jest: true,
  },
};
