const fs = require('fs');

module.exports = {
  '.dockerignore': require('./.dockerignore'),
  '.gitlab-ci.yml': require('./.gitlab-ci.yml'),
  Dockerfile: require('./Dockerfile'),
  'helpers.php': fs.readFileSync(`${__dirname}/helpers.php`),
};
