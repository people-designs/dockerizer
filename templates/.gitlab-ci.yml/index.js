const fs = require('fs');

module.exports = {
  instance: fs.readFileSync(`${__dirname}/instance`).toString(),
  project: fs.readFileSync(`${__dirname}/project`).toString(),
};
