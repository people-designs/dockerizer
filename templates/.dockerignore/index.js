const fs = require('fs');

module.exports = {
  common: fs.readFileSync(`${__dirname}/common`).toString(),
  'laravel-frontend': fs.readFileSync(`${__dirname}/laravel-frontend`).toString(),
  'laravel-frontend-elixir': fs.readFileSync(`${__dirname}/laravel-frontend-elixir`).toString(),
  'laravel-php': fs.readFileSync(`${__dirname}/laravel-php`).toString(),
};
