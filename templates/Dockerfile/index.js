const fs = require('fs');

module.exports = {
  'laravel-app': fs.readFileSync(`${__dirname}/laravel-app`).toString(),
  'laravel-app-elixir': fs.readFileSync(`${__dirname}/laravel-app-elixir`).toString(),
  'laravel-frontend': fs.readFileSync(`${__dirname}/laravel-frontend`).toString(),
  'laravel-frontend-elixir': fs.readFileSync(`${__dirname}/laravel-frontend-elixir`).toString(),
  'laravel-php': fs.readFileSync(`${__dirname}/laravel-php`).toString(),
};
