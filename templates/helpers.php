<?php

if (! function_exists('env_or_docker_secret')) {
    function env_or_docker_secret($key, $default = null)
    {
        $file = "/run/secrets/" . strtolower($key);
        if (file_exists($file)) {
            return trim(file_get_contents($file));
        }

        $value = getenv($key);
        if ($value === false) {
            return value($default);
        }
        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }
        if (($valueLength = strlen($value)) > 1 && $value[0] === '"' && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }
        return $value;
    }
}
