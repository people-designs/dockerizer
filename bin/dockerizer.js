#!/usr/bin/env node

const commander = require('commander');
const { version } = require('../package.json');

const dockerizer = require('../lib');

commander
  .version(version)
  .option('--dry-run', 'Don\'t execute any gcloud commands.')
  .option('--gitlab', 'Add Gitlab variables.')
  .option('--sql-connection [sql-connection]', 'SQL connection. Defaults to pd-secure:us-central1:sql-server-1')
  .option('--region [region]', 'Region for GCE. Defaults to us-central1.')
  .option('--sql', 'Create SQL database and user. Password is generated automatically.')
  .option('--verbose', 'Print command output.')
  .option('--zone [zone]', 'Zone for GCE. Defaults to us-central1-a.')
  .parse(process.argv);

dockerizer.run(commander);
